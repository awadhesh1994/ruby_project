class SessionController < ApplicationController

  def create
    auth = request.env['amniauth.rb']
    session[:omniauth] = auth.except('extra')
    user = User.sign_in_form_amniauth(auth)
    session[:userid] = user.id
    redirect_to root_url, notice: "Signed In"
  end

  def destroy
    session[:userid] = nil
    session[:omniauth] = nil
    redirect_to root_url, notice: "Siged Out"
  end

end
