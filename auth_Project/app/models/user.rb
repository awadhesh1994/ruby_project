class User < ApplicationRecord

    def sign_in_form_amniauth(auth)
        find_by(provider: auth['provider'], uid: auth['uid']) || create_user_from_omniauth(auth)
    end

    def create_user_from_omniauth
        create(
            provider: auth['provider'],
            uid: auth['uid'],
            name: auth['info']['name']
        )
    end

end
