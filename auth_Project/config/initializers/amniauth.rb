Rails.application.config.middleware.use OmniAuth::Builder do
    provider :facebook, ENV['TWITTER_KEY'], ENV['TWITTER_SECRET']
    provider :twitter, ENV['TWITTER_KEY'], ENV['TWITTER_SECRET']
    provider :github, ENV['TWITTER_KEY'], ENV['TWITTER_SECRET']
end