Rails.application.routes.draw do
  resources :blogs
  root 'homes#index'
  devise_for :users
end
